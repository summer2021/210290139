package org.apache.dolphinscheduler.dao.upgrade.overallshell;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.dolphinscheduler.dao.upgrade.DolphinSchedulerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CreateDolphinScheduler {
    private static final Logger logger = LoggerFactory.getLogger(CreateDolphinScheduler.class);
    public static void main(String[] args) {
        DolphinSchedulerManager dolphinSchedulerManager = new DolphinSchedulerManager();
        try {
            dolphinSchedulerManager.initDolphinScheduler("MYSQL");
            logger.info("init DolphinScheduler finished");
            //dolphinSchedulerManager.upgradeDolphinScheduler();
            logger.info("upgrade DolphinScheduler finished");
            logger.info("create DolphinScheduler success");
        } catch (Exception e) {
            logger.error("create DolphinScheduler failed",e);
        }
    }
}
